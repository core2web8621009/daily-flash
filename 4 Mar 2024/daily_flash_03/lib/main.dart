import 'package:daily_flash_03/program1.dart';
import 'package:daily_flash_03/program2.dart';
import 'package:daily_flash_03/program3.dart';
import 'package:daily_flash_03/program4.dart';
import 'package:daily_flash_03/program5.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Problem5(),
    );
  }
}
