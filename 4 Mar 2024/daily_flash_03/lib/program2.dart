/*
2. Create a Container in the Center of the screen, now In the background of
the Container display an Image (the image can be an asset image or
network image ). Also, display text in the center of the Container.
*/

import 'package:flutter/material.dart';

class Problem2 extends StatelessWidget {
  const Problem2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/image.jpg"),
            ),
          ),
          child: const Center(
            child: Text(
              "Hello Flutter",
              style: TextStyle(
                color: Colors.white,
                fontSize: 30,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
