/*
4. Create a Container with size(height:200, width:300) now give a shadow to
the container but the shadow must only be at the top side of the container.
*/

import 'package:flutter/material.dart';

class Problem4 extends StatelessWidget {
  const Problem4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: const BoxDecoration(
            color: Colors.yellow,
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                offset: Offset(0, -10),
                blurRadius: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
