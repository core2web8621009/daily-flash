/*
1. Create a Container in the Center of the Screen with size(width: 300,
height: 300) and display an image in the center of the Container. Apply
appropriate padding to the container.
*/

import 'package:flutter/material.dart';

class Problem1 extends StatelessWidget {
  const Problem1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          padding: const EdgeInsets.all(30),
          color: Colors.amber,
          child: Image.asset("images/image.jpg"),
        ),
      ),
    );
  }
}
