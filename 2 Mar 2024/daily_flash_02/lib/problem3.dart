/*
3. In the screen add a container of size( width 100, height: 100) . The container
must have a border as displayed in the below image. Give color to the container
and border as per your choice.
*/

import 'package:flutter/material.dart';

class Problem3 extends StatelessWidget {
  const Problem3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
            color: const Color.fromARGB(255, 208, 190, 240),
            border: Border.all(
              color: Colors.deepPurple,
              width: 5,
            ),
            borderRadius: const BorderRadius.only(
              topRight: Radius.circular(30),
            ),
          ),
        ),
      ),
    );
  }
}
