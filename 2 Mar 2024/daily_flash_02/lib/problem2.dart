/*
2. In the screen add a container of size( width 100, height: 100) that must only
have a left border of width 5 and color as per your choice. Give padding to the
container and display a text in the Container.
*/

import 'package:flutter/material.dart';

class Problem2 extends StatelessWidget {
  const Problem2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 100,
          width: 100,
          padding: const EdgeInsets.all(28),
          decoration: const BoxDecoration(
            color: Colors.amber,
            border: Border(
              left: BorderSide(
                color: Colors.deepPurple,
                width: 5,
              ),
            ),
          ),
          child: const Text("Hello Flutter"),
        ),
      ),
    );
  }
}
