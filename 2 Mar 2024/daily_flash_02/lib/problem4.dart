/*
4. Create a container that will have a border. The top right and bottom left corners
of the border must be rounded. Now display the Text in the Container and give
appropriate padding to the container.
*/

import 'package:flutter/material.dart';

class Problem4 extends StatelessWidget {
  const Problem4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 100,
          width: 300,
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: const Color.fromARGB(255, 255, 176, 176),
            border: Border.all(
              color: Colors.red,
              width: 5,
            ),
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          child: const Text("Your Name"),
        ),
      ),
    );
  }
}
