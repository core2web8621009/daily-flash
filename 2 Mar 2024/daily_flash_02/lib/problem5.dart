/*
5. Add a container with the color red and display the text "Click me!" in the center
of the container. On tapping the container, the text must change to “Container
Tapped” and the color of the container must change to blue.
*/

import 'package:flutter/material.dart';

class Problem5 extends StatefulWidget {
  const Problem5({super.key});

  @override
  State createState() => Problem5State();
}

class Problem5State extends State {
  bool isColorChange = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GestureDetector(
          onTap: () {
            setState(() {
              (isColorChange) ? isColorChange = false : isColorChange = true;
            });
          },
          child: Container(
            height: 200,
            width: 300,
            color: (isColorChange) ? Colors.red : Colors.blue,
            child: Center(
              child: (isColorChange)
                  ? const Text(
                      "Click me!",
                      style: TextStyle(fontSize: 25),
                    )
                  : const Text(
                      "Container Tapped",
                      style: TextStyle(fontSize: 25),
                    ),
            ),
          ),
        ),
      ),
    );
  }
}
