/*
1. Create a Screen and try to replicate the provided diagram. Customize the UI to
include containers with different colors, providing each container with
appropriate width and height dimensions as shown. Ensure proper margins as
depicted in the provided diagram, using colors and dimensions of your choice.
*/

import 'package:flutter/material.dart';

class Problem1 extends StatelessWidget {
  const Problem1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: const [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: Colors.blue,
              radius: 30,
            ),
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 150,
                width: 150,
                color: Colors.amber,
              ),
              Container(
                height: 150,
                width: 150,
                color: Colors.red,
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 150,
                width: 300,
                color: Colors.green,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 150,
                width: 150,
                color: Colors.purple,
              ),
              Container(
                height: 150,
                width: 150,
                color: Colors.blue,
              )
            ],
          ),
        ],
      ),
    );
  }
}
