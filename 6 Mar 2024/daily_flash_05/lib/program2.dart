/*
2. Create a Screen in which we have 3 Containers in a Column each container
must be of height 100 and width 100. Each container must have an image
as a child.
*/

import 'package:flutter/material.dart';

class Problem2 extends StatelessWidget {
  const Problem2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 100,
            width: 100,
            child: Image.network(
                "https://img.mensxp.com/media/content/2023/Sep/Image-2_Orion-Pictures_64f409bfc2730.jpeg"),
          ),
          SizedBox(
            height: 100,
            width: 100,
            child: Image.network(
                "https://img.mensxp.com/media/content/2023/Sep/Image-2_Orion-Pictures_64f409bfc2730.jpeg"),
          ),
          SizedBox(
            height: 100,
            width: 100,
            child: Image.network(
                "https://img.mensxp.com/media/content/2023/Sep/Image-2_Orion-Pictures_64f409bfc2730.jpeg"),
          ),
        ],
      ),
    );
  }
}
