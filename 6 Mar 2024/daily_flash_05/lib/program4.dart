/*
4. Create a Screen in which we will display 3 Containers of Size 100,100 in a
Row. Give color to the containers. The containers must divide the free
space in the main axis evenly among each other.
*/

import 'package:flutter/material.dart';

class Problem4 extends StatelessWidget {
  const Problem4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 100,
                width: 100,
                child: Image.network(
                    "https://img.mensxp.com/media/content/2023/Sep/Image-2_Orion-Pictures_64f409bfc2730.jpeg"),
              ),
              SizedBox(
                height: 100,
                width: 100,
                child: Image.network(
                    "https://img.mensxp.com/media/content/2023/Sep/Image-2_Orion-Pictures_64f409bfc2730.jpeg"),
              ),
              SizedBox(
                height: 100,
                width: 100,
                child: Image.network(
                    "https://img.mensxp.com/media/content/2023/Sep/Image-2_Orion-Pictures_64f409bfc2730.jpeg"),
              )
            ],
          ),
        ],
      ),
    );
  }
}
