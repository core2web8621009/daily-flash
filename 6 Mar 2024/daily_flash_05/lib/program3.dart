/*
3. Create a Screen and add your image in the center of the screen below your
image display your name in a container, give a shadow to the Container
and give a border to the container the top left and top right corners must
be circular, with a radius of 20. Add appropriate padding to the container.
*/

import 'package:flutter/material.dart';

class Problem3 extends StatelessWidget {
  const Problem3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 550,
              width: 500,
              child: Image.network(
                  "https://img.mensxp.com/media/content/2023/Sep/Image-2_Orion-Pictures_64f409bfc2730.jpeg"),
            ),
            Container(
              height: 50,
              width: 200,
              decoration: const BoxDecoration(
                color: Colors.amber,
                boxShadow: [
                  BoxShadow(
                      color: Colors.black, blurRadius: 10, offset: Offset(5, 5))
                ],
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              child: const Center(
                child: Text("Salman Khan"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
