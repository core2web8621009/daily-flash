/*
5. Create a Screen that displays 3 widgets in a Column. The image must be the
first widget, the next widget must be a Container of color red and the 3rd
widget must be a Container of color blue. Place all the 3 widgets in a
Column.
The Image must be placed at the top center and the other 2 widgets must
be placed at the bottom center of the screen.
*/

import 'package:flutter/material.dart';

class Problem5 extends StatelessWidget {
  const Problem5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 200,
            width: 200,
            child: Image.network(
                "https://img.mensxp.com/media/content/2023/Sep/Image-2_Orion-Pictures_64f409bfc2730.jpeg"),
          ),
          const Spacer(),
          Container(
            height: 200,
            width: 200,
            color: Colors.red,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 200,
            width: 200,
            color: Colors.blue,
          ),
        ],
      ),
    );
  }
}
