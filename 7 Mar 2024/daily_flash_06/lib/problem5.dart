/*
5. Create a Screen in which we have 3 Containers with size:
(height:100,width:200) placed vertically. Each container must have a
black border. Initially, the Color of the Containers must be white. The
container that is tapped must change its color to red and other containers
must be white.
*/

import 'package:flutter/material.dart';

class Problem5 extends StatefulWidget {
  const Problem5({super.key});

  @override
  State createState() => _Problem5State();
}

class _Problem5State extends State {
  bool color = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash"),
        backgroundColor: Colors.blue,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          const SizedBox(
            width: 390,
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                color = true;
              });
            },
            child: Container(
              height: 130,
              width: 150,
              decoration: BoxDecoration(
                color: (color) ? Colors.white : Colors.red,
                border: Border.all(
                  color: Colors.black,
                  width: 3,
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                color = true;
              });
            },
            child: Container(
              height: 130,
              width: 150,
              decoration: BoxDecoration(
                color: (color) ? Colors.red : Colors.red,
                border: Border.all(
                  color: Colors.black,
                  width: 3,
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                color = true;
              });
            },
            child: Container(
              height: 130,
              width: 150,
              decoration: BoxDecoration(
                color: (color) ? Colors.white : Colors.red,
                border: Border.all(
                  color: Colors.black,
                  width: 3,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
