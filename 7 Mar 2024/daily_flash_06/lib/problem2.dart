/*
2. Create a screen that displays a container. The container must display an image.
Give a circular border only at the bottom of the container. Below the container
display the button with size:(width:250, height:70). The button must display
“Add to cart”. The color of the button must be purple. Both the container and
button must be in the center of the screen.
*/

import 'package:flutter/material.dart';

class Problem2 extends StatelessWidget {
  const Problem2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash"),
        backgroundColor: Colors.blue,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Row(),
          Container(
            height: 300,
            width: 300,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30),
              ),
            ),
            child: Image.network(
              "https://recipesblob.oetker.in/assets/d8a4b00c292a43adbb9f96798e028f01/1272x764/pizza-pollo-arrostojpg.jpg",
              fit: BoxFit.fill,
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          SizedBox(
            height: 70,
            width: 250,
            child: ElevatedButton(
              style: const ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(Colors.purple),
              ),
              onPressed: () {},
              child: const Text(
                "Add To Cart",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
