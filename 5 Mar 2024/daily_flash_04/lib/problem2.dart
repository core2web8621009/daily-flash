/*
2. Create an Elevated button in the Center of the Screen. Decorate the button as
follows.
a. The button must be of Circular Shape.
b. The Size of the button must be (width:200, height: 200).
c. The button must have a border of color red.
*/

import 'package:flutter/material.dart';

class Problem2 extends StatelessWidget {
  const Problem2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SizedBox(
          height: 200,
          width: 200,
          child: ElevatedButton(
            style: const ButtonStyle(
              shadowColor: MaterialStatePropertyAll(Colors.red),
              shape: MaterialStatePropertyAll(
                CircleBorder(
                  side: BorderSide(
                    color: Colors.red,
                  ),
                ),
              ),
            ),
            onPressed: () {},
            child: const Text("Elevated Button"),
          ),
        ),
      ),
    );
  }
}
