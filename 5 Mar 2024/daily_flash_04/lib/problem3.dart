/*
3. Create a Screen and then add a floating action button. In this button, you
will have to display your name and an Icon which must be placed in a row.
*/

import 'package:flutter/material.dart';

class Problem3 extends StatelessWidget {
  const Problem3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const Row(
        children: [
          Text("Dhiraj"),
          Icon(Icons.home),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
    );
  }
}
