/*
1. Create an ElevatedButton, in the centre of the screen. The button must
have rounded edges. Give a shadow of color red to the button.
*/

import 'package:flutter/material.dart';

class Problem1 extends StatelessWidget {
  const Problem1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          style: const ButtonStyle(
            shadowColor: MaterialStatePropertyAll(Colors.red),
          ),
          onPressed: () {},
          child: const Text("Elevated Button"),
        ),
      ),
    );
  }
}
