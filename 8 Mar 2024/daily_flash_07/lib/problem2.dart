/*
2. Create a Screen that displays an `Icon` widget representing a star the size of the
icon must be 40 and the color of the icon must be orange, beside the icon place a
`Text` widget with the content "Rating: 4.5". Apply a font size of 25 and bold
weight to the text. Refer to below image.
*/

import 'package:flutter/material.dart';

class Problem2 extends StatelessWidget {
  const Problem2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Container(
          height: 80,
          width: 250,
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.black,
              width: 2.3,
            ),
            borderRadius: BorderRadius.circular(20),
          ),
          child: const Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Icon(
                Icons.star,
                size: 40,
              ),
              Text(
                "Rating: 4.5",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
