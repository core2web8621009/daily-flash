/*
3. Create a Screen with two horizontally aligned containers at the center of the
screen. Apply a shadow to each container set individual colors and give a border
to the Containers only the bottom edges of the container must be rounded.
*/

import 'package:flutter/material.dart';

class Problem3 extends StatelessWidget {
  const Problem3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily Flash"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 150,
                width: 150,
                decoration: BoxDecoration(
                  color: Colors.red,
                  border: Border.all(
                    color: Colors.black,
                    width: 2.3,
                  ),
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                  boxShadow: const [
                    BoxShadow(
                        blurRadius: 20,
                        color: Colors.black,
                        offset: Offset(10, 10))
                  ],
                ),
              ),
            ),
            Container(
              height: 150,
              width: 150,
              decoration: BoxDecoration(
                color: Colors.purple,
                border: Border.all(
                  color: Colors.black,
                  width: 2.3,
                ),
                borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
                boxShadow: const [
                  BoxShadow(
                      blurRadius: 20,
                      color: Colors.black,
                      offset: Offset(10, 10))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
