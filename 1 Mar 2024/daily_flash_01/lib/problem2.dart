/*
2. Create an AppBar give a color of your choice to the AppBar and then
add an icon at the start of the AppBar and 3 icons at the end of the
AppBar.
*/

import 'package:flutter/material.dart';

class Problem2 extends StatefulWidget {
  const Problem2({super.key});

  @override
  State createState() => _Problem2State();
}

class _Problem2State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Text("App Bar"),
        centerTitle: true,
        toolbarHeight: 70,
        leading: const Icon(
          Icons.alternate_email_sharp,
          size: 35,
        ),
        actions: const [
          Icon(
            Icons.android,
            size: 35,
          ),
          Icon(
            Icons.apple,
            size: 35,
          ),
          Icon(
            Icons.yard_outlined,
            size: 35,
          ),
        ],
      ),
    );
  }
}
