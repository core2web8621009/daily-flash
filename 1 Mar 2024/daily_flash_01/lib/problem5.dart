/*
5. Create a Screen, in the center of the Screen display a Container with
rounded corners, give a specific color to the Container, the container
must have a shadow of color red.
*/

import 'package:flutter/material.dart';

class Problem5 extends StatefulWidget {
  const Problem5({super.key});

  @override
  State createState() => _Problem5State();
}

class _Problem5State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.blue,
              boxShadow: const [
                BoxShadow(
                  color: Colors.red,
                  blurRadius: 20,
                  offset: Offset(10, 10),
                )
              ]),
        ),
      ),
    );
  }
}
