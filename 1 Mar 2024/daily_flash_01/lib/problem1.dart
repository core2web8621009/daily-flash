/*
1. Create an AppBar, give an Icon at the start of the appbar, give a title
in the middle, and at the end add an Icon.
*/

import 'package:flutter/material.dart';

class Problem1 extends StatefulWidget {
  const Problem1({super.key});

  @override
  State createState() => _Problem1State();
}

class _Problem1State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("App Bar"),
        centerTitle: true,
        toolbarHeight: 70,
        leading: const Icon(
          Icons.apple,
          size: 35,
        ),
        actions: const [
          Icon(
            Icons.android,
            size: 35,
          ),
        ],
      ),
    );
  }
}
