import 'package:daily_flash_01/problem3.dart';
import 'package:daily_flash_01/problem4.dart';
import 'package:daily_flash_01/problem5.dart';
import 'package:flutter/material.dart';
import 'package:daily_flash_01/problem1.dart';
import 'package:daily_flash_01/problem2.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Problem5(),
    );
  }
}
