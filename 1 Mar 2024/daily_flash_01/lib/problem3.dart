/*
3. Create a Screen that will display an AppBar. Add a title in the AppBar
the app bar must have a round rectangular border at the bottom.
*/

import 'package:flutter/material.dart';

class Problem3 extends StatefulWidget {
  const Problem3({super.key});

  @override
  State createState() => _Problem3State();
}

class _Problem3State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("App Bar"),
        centerTitle: true,
        toolbarHeight: 70,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(25),
            bottomRight: Radius.circular(25),
          ),
        ),
      ),
    );
  }
}
